import qs from 'qs'
import React from 'react'

const TestComponent = () => {

    return (
        <>
            {qs.stringify({foo: 'bar'})}
            TestComponent A
        </>
    )
}

export default TestComponent
